//
//  main.m
//  Tests
//
//  Created by John Kroubalkian on 7/6/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GHUnitIOS/GHUnitIOSAppDelegate.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GHUnitIOSAppDelegate class]));
        //        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MyTestableAppAppDelegate class]));
    }
}
