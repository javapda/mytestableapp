//
//  MyTest.m
//  MyTestableApp
//
//  Created by John Kroubalkian on 7/6/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
//

#import <GHUnitIOS/GHUnit.h> 
#import "SomeEngine.h"
#import "CombinedTargetClass.h"
@interface MyTest : GHTestCase { }
@end

@implementation MyTest
-(void) testCombinedTargetClassNow
{
    CombinedTargetClass *ctc=[[CombinedTargetClass alloc]init];
    NSLog(@"Type: %@",[ctc getType]);
}
- (void) testSomeEngine
{
    SomeEngine *se = [[SomeEngine alloc]init];
    NSLog(@"Some engine: %@",se);
}
- (void)testStrings {       
    NSString *string1 = @"a string";
    GHTestLog(@"I can log to the GHUnit test console: %@", string1);
    
    // Assert string1 is not NULL, with no custom error description
    //GHAssertNotNULL(string1, @"SOME DES");
    GHAssertNotNil(string1, nil);
    GHAssertNotNil(string1, @"Was not expecing a nil");
    // Assert equal objects, add custom error description
    NSString *string2 = @"a string";
    GHAssertEqualObjects(string1, string2, @"A custom error message. string1 should be equal to: %@.", string2);
}

@end
