//
//  MyTestableAppViewController.h
//  MyTestableApp
//
//  Created by John Kroubalkian on 7/6/12.
//  Copyright (c) 2012 Jed Clampett Dining and Fishing, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTestableAppViewController : UIViewController

@end
